import React, { useEffect } from 'react';
import { View, Image, StyleSheet, ActivityIndicator } from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
} from 'react-native-reanimated';
import { useNavigation } from '@react-navigation/native';

const FirstScreen = () => {
  const logoY = useSharedValue(400);
  const navigation = useNavigation();

  useEffect(() => {
    logoY.value = withSpring(0, {
      damping: 10,
      stiffness: 100,
    });

    const timer = setTimeout(() => {
      navigation.replace('Login');
    }, 5000);

    return () => clearTimeout(timer);
  }, [navigation, logoY]);

  const logoStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateY: logoY.value }],
    };
  });

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.logoContainer, logoStyle]}>
        <Image
          source={require('../assets/images/primero.png')}
          style={styles.logo}
        />
      </Animated.View>
      <ActivityIndicator size="large" color="#009688" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 300,
    height: 300,
  },
  
});

export default FirstScreen;
