import React from "react";
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import Onboarding from 'react-native-onboarding-swiper';
import LottieView from 'lottie-react-native';
import { useNavigation } from "@react-navigation/native";

const { width, height } = Dimensions.get('window');

export default function OnboardingScreen() {
    const navigation = useNavigation();

    const handleDone = () => {
        navigation.navigate('Home');
    }

    const doneButton = ({...props}) => {
        return (
            <TouchableOpacity style={styles.doneButton} {...props}>
                <Text>Done</Text>
            </TouchableOpacity>
        );
    }

    return (
       <View style={styles.container}>
    <Onboarding
        onDone={handleDone}
        onSkip={handleDone}
        DoneButtonComponent={doneButton}
        containerStyles={{ paddingHorizontal: 15 }}
        pages={[
            {
                backgroundColor: '#009688', // Teal
                image: (
                    <View style={styles.lottie}>
                        <LottieView source={require('../assets/animations/calendar.json')} autoPlay loop style={styles.lottie}/>
                    </View>
                ),
                title: 'Welcome to Our Event Management App!',
                subtitle: 'Create events, explore hotels, venues, and transportation, select your preferences, and add guests for a comprehensive experience.',
            },
            {
                backgroundColor: '#00796B', // Tropical Rain Forest
                image: (
                    <View style={styles.lottie}>
                        <LottieView source={require('../assets/animations/bars.json')} autoPlay loop style={styles.lottie} />
                    </View>
                ),
                title: 'Seamless Onboarding Experience',
                subtitle: 'Discover our event creation tools and effortlessly organize all the details.',
            },
            {
                backgroundColor: '#004D40', // British Racing Green
                image: (
                    <View style={styles.lottie}>
                        <LottieView source={require('../assets/animations/CONFETTI.json')} autoPlay loop style={styles.lottie}/>
                    </View>
                ),
                title: 'Start Exploring Now!',
                subtitle: 'Begin enjoying the experience of efficiently and stylishly organizing corporate events.',
            },
        ]}
    />
</View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    lottieContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    lottie: {
        width: width * 0.9,
        height: width * 0.9
    },
    doneButton: {
        padding: 20,
    },
});




