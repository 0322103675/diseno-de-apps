import React, { useState, useEffect, useRef } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Animated, Image, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import API_URL from '../../apiConfig';

export default function UserProfile() {
  const fadeIn = useRef(new Animated.Value(0)).current;
  const fadeOut = useRef(new Animated.Value(1)).current;
  const slideUp = useRef(new Animated.Value(100)).current;
  const slideDown = useRef(new Animated.Value(0)).current;
  const [image, setImage] = useState(null);
  const [userInfo, setUserInfo] = useState(null); 
  const [editingName, setEditingName] = useState(false);
  const [newName, setNewName] = useState('');

  useEffect(() => {
    cargarDatosUsuario(); 
  }, []);

  const fadeInAnimation = () => {
    Animated.parallel([
      Animated.timing(fadeIn, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      }),
      Animated.timing(slideUp, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true
      })
    ]).start();
  };

  const fadeOutAnimation = () => {
    Animated.parallel([
      Animated.timing(fadeOut, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true
      }),
      Animated.timing(slideDown, {
        toValue: 100,
        duration: 500,
        useNativeDriver: true
      })
    ]).start();
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.assets[0].uri);
      fadeInAnimation();
    }
  };

  const handleNameEdit = () => {
    setEditingName(true);
    setNewName(userInfo.nombre);
  };

  const saveName = async () => {
    try {
      const response = await fetch(`${API_URL}/cliente/43`, {
        method: 'Put',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ nombre: newName })
      });

      if (response.ok) {
        setUserInfo({ ...userInfo, nombre: newName });
        setEditingName(false);
        fadeOutAnimation();
      } else {
        console.error('Error al guardar el nombre en la base de datos');
      }
    } catch (error) {
      console.error('Error al guardar el nombre en la base de datos:', error);
    }
  };

  const municipiosCompletos = {
    ENS: 'Ensenada',
    MEX: 'Mexicali',
    ROS: 'Rosarito',
    TEC: 'Tecate',
    TIJ: 'Tijuana'
  };

  const cargarDatosUsuario = async () => {
    try {
      const response = await fetch(`${API_URL}/cliente/43`);
      const data = await response.json();
      data.municipio = municipiosCompletos[data.municipio] || data.municipio;
      setUserInfo(data);
    } catch (error) {
      console.error('Error al obtener datos de usuario:', error);
    }
  };

  return (
    <KeyboardAvoidingView
      style={{ flex: 1, backgroundColor: 'white' }}
      behavior={Platform.OS === 'ios' ? 'padding' : null}
    >
      <View style={styles.container}>
        <Text style={styles.heading}>Profile</Text>
        <TouchableOpacity onPress={pickImage}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Select profile picture</Text>
          </View>
        </TouchableOpacity>
        <Animated.View style={[styles.imageContainer, { opacity: fadeIn, transform: [{ translateY: slideUp }] }]}>
          {image && <Image source={{ uri: image }} style={styles.image} />}
        </Animated.View>
        <View style={styles.userInfo}>
          {userInfo && (
            <>
              <View style={styles.infoItem}>
                <Text style={styles.label}>Name:</Text>
                {editingName ? (
                  <>
                    <TextInput
                      style={styles.input}
                      value={newName}
                      onChangeText={setNewName}
                      autoFocus
                    />
                    <TouchableOpacity style={styles.saveButton} onPress={saveName}>
                      <Text style={styles.saveButtonText}>Save</Text>
                    </TouchableOpacity>
                  </>
                ) : (
                  <>
                    <Text style={styles.info}>{userInfo.nombre}                 </Text>
                    <TouchableOpacity style={styles.editButton} onPress={handleNameEdit}>
                      <AntDesign name="edit" size={22} color="white" />
                    </TouchableOpacity>
                  </>
                )}
              </View>
              <InfoItem label="Address" info={`${userInfo.dir_calle}, ${userInfo.dir_num}, ${userInfo.dir_colonia}, ${userInfo.municipio}`} />
              <InfoItem label="Phone Number" info={userInfo.num_tel} />
              <InfoItem label="Email" info={userInfo.correo} />
            </>
          )}
        </View>
      </View>
    </KeyboardAvoidingView>
  );
}

const InfoItem = ({ label, info }) => (
  <View style={styles.infoItem}>
    <Text style={styles.label}>{label}:</Text>
    <Text style={styles.info}>{info}</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 20,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#009688',
    paddingVertical: 15,
    paddingHorizontal: 40,
    borderRadius: 8,
    marginBottom: 20,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  imageContainer: {
    width: 200,
    height: 200,
    borderRadius: 10,
    overflow: 'hidden',
    marginBottom: 20,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  userInfo: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 10,
    padding: 15,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '100%',
    alignItems: 'flex-start',
  },
  infoItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  label: {
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#009688',
  },
  info: {
    fontWeight: 'bold',
    color: 'black',
  },
  editButton: {
    backgroundColor: 'black',
    padding: 8,
    borderRadius: 5,
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 5,
    marginBottom: 5,
  },
  saveButton: {
    backgroundColor: '#28a745',
    padding: 8,
    borderRadius: 5,
  },
  saveButtonText: {
    color: 'white',
  },
});
