import React, { useState, useEffect } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import API_URL from '../../apiConfig';
import * as Animatable from 'react-native-animatable';

import Image1 from '../../assets/paquetes/basico.png';
import Image2 from '../../assets/paquetes/regular.png';
import Image3 from '../../assets/paquetes/premium.png';
import Image4 from '../../assets/paquetes/exclusivo.png';

const PackagesScreen = () => {
  const [packages, setPackages] = useState([]);
  const [expandedIndex, setExpandedIndex] = useState(null);

  useEffect(() => {
    fetchPackages();
  }, []);

  const fetchPackages = async () => {
    try {
      const response = await fetch(`${API_URL}/servicios_paquetes`);
      const data = await response.json();
      setPackages(data);
    } catch (error) {
      console.error('Error fetching packages:', error);
      // You could show an alert or handle the error in some way here
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      // Animation for the subheading
      setTimeout(() => {
        if (subHeadingRef.current) {
          subHeadingRef.current.bounceIn(1000);
        }
      }, 100);
    }, [])
  );

  const subHeadingRef = React.useRef(null);

  const handleDescriptionToggle = (index) => {
    if (expandedIndex === index) {
      setExpandedIndex(null);
    } else {
      setExpandedIndex(index);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.bottomSpace}></View>
      <Text style={styles.heading}>Packages List</Text>
      <Animatable.Text ref={subHeadingRef} animation="bounceIn" style={styles.subHeading}>Check out your options!</Animatable.Text>
      {packages.map((pkg, index) => (
        <Animatable.View key={pkg.numero} animation="fadeInUp" delay={index * 100} style={styles.packageContainer}>
          <Image source={getImageSource(index)} style={styles.packageImage} />
          <Text style={styles.packageName}>{pkg.nombre}</Text>
          <TouchableOpacity onPress={() => handleDescriptionToggle(index)} style={styles.descriptionButton}>
            <Text style={styles.descriptionButtonText}>Description</Text>
          </TouchableOpacity>
          {expandedIndex === index && (
            <Animatable.View animation="fadeIn" style={styles.descriptionContainer}>
              <Text style={styles.packageDescription}>{pkg.descripcion}</Text>
              <Text style={styles.packagePrice}>Price: {pkg.precio}</Text>
            </Animatable.View>
          )}
        </Animatable.View>
      ))}
      {/* Additional space for scrolling */}
      <View style={styles.bottomSpace}></View>
    </ScrollView>
  );
};

const getImageSource = (index) => {
  switch (index) {
    case 0:
      return Image1;
    case 1:
      return Image2;
    case 2:
      return Image3;
    case 3:
      return Image4;
    default:
      return null;
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#ffffff',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  subHeading: {
    fontSize: 24,
    marginBottom: 20,
    textAlign: 'center',
    color: '#009688',
    fontWeight: 'bold',
  },
  packageContainer: {
    marginBottom: 30,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: '#ffffff',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  packageImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  packageName: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 5,
    textAlign: 'center',
  },
  descriptionButton: {
    backgroundColor: 'rgba(0, 150, 136, 0.7)',
    paddingVertical: 8,
    paddingHorizontal: 20,
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: 10,
  },
  descriptionButtonText: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 16,
  },
  descriptionContainer: {
    paddingHorizontal: 15,
    marginTop: 10,
  },
  packageDescription: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: 'center',
  },
  packagePrice: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#009688',
  },
  bottomSpace: {
    paddingBottom: 80, // Additional space at the end of the content
  },
});

export default PackagesScreen;
