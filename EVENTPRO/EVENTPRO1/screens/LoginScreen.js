import React, { useState } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import Animated, { FadeIn, FadeInDown, FadeInUp } from 'react-native-reanimated';
import { useNavigation } from '@react-navigation/native';
import API_URL from '../apiConfig'; // Importa la URL base de la API
import { Ionicons } from '@expo/vector-icons';

export default function LoginScreen() {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const handleLogin = async () => {
    try {
      // Validación de email
      if (!email || !email.includes('@')) {
        Alert.alert('Error', 'Please enter a valid email address');
        return;
      }

      // Validación de contraseña
      if (!password) {
        Alert.alert('Error', 'Please enter your password');
        return;
      }

      const response = await fetch(`${API_URL}/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, password })
      });

      if (!response.ok) {
        const errorData = await response.json();
        if (errorData.error === 'invalid_credentials') {
          Alert.alert('Error:', 'Incorrect email or password');
        } else {
          throw new Error('Authentication failed');
        }
      } else {
        const data = await response.json();
        Alert.alert('Welcome to EVENTPRO');
        navigation.navigate('Onboarding');
      }
    } catch (error) {
      console.error('Error al iniciar sesión:', error);
      Alert.alert('Error:', 'An error occurred while logging in');
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <StatusBar style="light" />

      <Image
        style={{ flex: 1, position: 'absolute', width: '100%', height: '100%' }}
        source={require('../assets/images/home7.jpeg')}
        resizeMode="cover"
      />

      <View style={{ flexDirection: 'row', justifyContent: 'space-around', position: 'absolute', width: '100%' }}>
        <Animated.Image
          entering={FadeInUp.delay(200).duration(1000).springify().damping(3)}
          style={{ height: 245, width: 99 }}
          source={require('../assets/images/light.png')}
        />
        <Animated.Image
          entering={FadeInUp.delay(400).duration(1000).springify().damping(3)}
          style={{ height: 160, width: 65 }}
          source={require('../assets/images/light.png')}
        />
      </View>

      <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 20 }}>
        <View style={{ alignItems: 'center', marginBottom: 40 }}>
          <Animated.View entering={FadeInUp.duration(1000).springify()}>
            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 51 }}>Login</Text>
          </Animated.View>
        </View>

        <View style={{ marginBottom: 20 }}>
          <Animated.View entering={FadeInDown.delay(200).duration(1000).springify()} style={{ marginBottom: 20 }}>
            <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20 }}>
              <TextInput placeholder="Email" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setEmail} value={email} />
            </View>
          </Animated.View>

          <Animated.View entering={FadeInDown.delay(400).duration(1000).springify()} style={{ marginBottom: 20 }}>
            <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, flexDirection: 'row', alignItems: 'center' }}>
              <TextInput placeholder="Password" placeholderTextColor={'white'} secureTextEntry={!showPassword} style={{ flex: 1, color: 'white' }} onChangeText={setPassword} value={password} />
              <TouchableOpacity onPress={() => setShowPassword(!showPassword)} style={{ paddingHorizontal: 10 }}>
                <Ionicons name={showPassword ? 'eye-off' : 'eye'} size={24} color="white" />
              </TouchableOpacity>
            </View>
          </Animated.View>

          <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()}>
            <TouchableOpacity onPress={handleLogin} style={{ backgroundColor: '#4ECDC4', padding: 15, borderRadius: 20, marginBottom: 20 }}>
              <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Login</Text>
            </TouchableOpacity>
          </Animated.View>

          <Animated.View entering={FadeInDown.delay(800).duration(1000).springify()} style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ color: 'white' }}>Don't have an account? </Text>
            <TouchableOpacity onPress={() => navigation.push('SignUp')}>
              <Text style={{ color: 'black' }}>Sign Up</Text>
            </TouchableOpacity>
          </Animated.View>
        </View>
      </View>
    </View>
  );
}
