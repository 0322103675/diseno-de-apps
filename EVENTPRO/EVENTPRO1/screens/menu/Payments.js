import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; 
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';

const Payments = ({ navigation }) => {
  const [paymentCompleted, setPaymentCompleted] = useState(false);

  const handlePayment = () => {
    setTimeout(() => {
      setPaymentCompleted(true);
      Alert.alert('Payment Completed', 'The payment has been successfully processed!');
    }, 3000);
  };

  return (
    <DrawerSceneWrapper>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.menuButton}>
          <Ionicons name="menu" size={26} color="#009688" />
        </TouchableOpacity>
        {paymentCompleted ? (
          <Text style={styles.paymentText}>Payment successfully completed!</Text>
        ) : (
          <TouchableOpacity style={styles.button} onPress={handlePayment}>
            <Text style={styles.buttonText}>Make Payment</Text>
          </TouchableOpacity>
        )}
      </View>
    </DrawerSceneWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: '#009688',
    padding: 15,
    borderRadius: 5,
    marginBottom: 20,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  paymentText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  menuButton: {
    position: 'absolute',
    top: 35,
    left: 20,
    zIndex: 1,
  },
});

export default Payments;
