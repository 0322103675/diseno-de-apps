import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Animated, Easing } from 'react-native';
import LottieView from 'lottie-react-native';

const LogoutScreen = ({ navigation }) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      navigation.replace('Login');
    }, 3000);

    return () => clearTimeout(timer);
  }, [navigation]);

  const opacity = new Animated.Value(0);
  const scale = new Animated.Value(0);
  const translateY = new Animated.Value(50);
  const rotate = new Animated.Value(0);
  const confettiOpacity = new Animated.Value(0);

  const fadeIn = Animated.timing(opacity, {
    toValue: 1,
    duration: 1500,
    useNativeDriver: true,
  });

  const scaleIn = Animated.spring(scale, {
    toValue: 1,
    friction: 2,
    tension: 120,
    useNativeDriver: true,
  });

  const translateYIn = Animated.timing(translateY, {
    toValue: 0,
    duration: 1000,
    easing: Easing.out(Easing.exp),
    useNativeDriver: true,
  });

  const rotateIn = Animated.loop(
    Animated.timing(rotate, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear,
      useNativeDriver: true,
    })
  );

  const confettiIn = Animated.timing(confettiOpacity, {
    toValue: 1,
    duration: 2000,
    delay: 1000,
    useNativeDriver: true,
  });

  Animated.parallel([fadeIn, scaleIn, translateYIn, rotateIn, confettiIn]).start();

  const spin = rotate.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  return (
    <View style={styles.container}>
      <Animated.Text style={[styles.text, { opacity, transform: [{ scale }, { translateY }, { rotate: spin }] }]}>Goodbye</Animated.Text>
      <Animated.View style={[styles.confettiContainer, { opacity: confettiOpacity }]}>
        <LottieView
          source={require('../../assets/animations/CONFETTI.json')}
          autoPlay
          loop
          style={styles.confetti}
        />
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white', 
  },
  text: {
    fontSize: 45, 
    fontWeight: 'bold',
    color: '#009688', 
    marginBottom: 20, 
  },
  confettiContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  confetti: {
    width: 300, // Tamaño de la animación aumentado
    height: 300, // Tamaño de la animación aumentado
  },
});

export default LogoutScreen;
