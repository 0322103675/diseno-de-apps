import React, { useState } from 'react';
import { View, Text, TextInput, Linking, StyleSheet, Alert, Keyboard, TouchableOpacity, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, Platform } from 'react-native'; // Asegúrate de importar Platform aquí

import { Ionicons } from '@expo/vector-icons';
import * as Animatable from 'react-native-animatable';
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';

const CommentsScreen = ({ navigation }) => {
  const [comment, setComment] = useState('');
  const [subject, setSubject] = useState('');

  const sendEmail = async () => {
    const email = 'eventpro4d@gmail.com';
    const body = comment;
    const url = `mailto:${email}?subject=${encodeURIComponent(subject)}&body=${encodeURIComponent(body)}`;

    try {
      await Linking.openURL(url);
     
      setComment('');
      setSubject('');

      Alert.alert(
        'Email Sent',
        'Your comment has been sent. Thank you for your feedback!',
        [{ text: 'OK' }]
      );
    } catch (error) {
      console.error('Error sending email:', error);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <DrawerSceneWrapper>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
          <ScrollView contentContainerStyle={styles.scrollViewContent}>
            <View style={styles.container}>
              <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.drawerButton}>
                  <Ionicons name="menu" size={24} color="#009688" />
                </TouchableOpacity>
                <Text style={styles.title}>Leave a Comment</Text>
              </View>
              <Animatable.View animation="bounceIn" style={styles.imageContainer}>
                <Animatable.Image animation="rubberBand" source={require('../../assets/images/primero.png')} style={styles.logo} />
              </Animatable.View>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.input}
                  placeholder="How was your experience?"
                  onChangeText={text => setSubject(text)}
                  value={subject}
                  placeholderTextColor="#5C6969" 
                />
                <TextInput
                  style={styles.input}
                  placeholder="Give me your point of view"
                  onChangeText={text => setComment(text)}
                  value={comment}
                  placeholderTextColor="#5C6969" 
                />
              </View>
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={sendEmail}
                >
                  <Text style={styles.buttonText}>Send Email</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </DrawerSceneWrapper>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingTop: 40,
  },
  scrollViewContent: {
    flexGrow: 1,
    justifyContent: 'space-between',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  drawerButton: {
    marginRight: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  imageContainer: {
    alignItems: 'center',
  },
  logo: {
    width: 250,
    height: 250, 
    marginBottom: 0,
  },
  inputContainer: {
    marginBottom: 20,
  },
  input: {
    height: 100,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#009688',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default CommentsScreen;
