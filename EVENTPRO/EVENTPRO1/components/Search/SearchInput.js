import React, { useState } from 'react';
import { TextInput, View, StyleSheet } from 'react-native';
import Icon from '../shared/Icon';
import { colors, spacing, sizes, shadow } from '../../constants/theme';

const SearchInput = ({ onSearch }) => {
  const [search, setSearch] = useState('');

  const handleSearch = (text) => {
    setSearch(text);
    onSearch(text);
  };

  return (
    <View style={styles.container}>
      <View style={styles.inner}>
        <Icon icon="Search" style={styles.searchIcon} />
        <TextInput
          style={[styles.field, { color: 'black' }]}
          placeholderTextColor={colors.gray}
          placeholder="Search"
          value={search}
          onChangeText={handleSearch}
        />
        <Icon icon="Filter" style={styles.filterIcon} onPress={() => {}} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: spacing.l,
    paddingTop: spacing.l,
    paddingBottom: spacing.l / 1.5,
  },
  inner: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
  },
  searchIcon: {
    position: 'absolute',
    left: spacing.s,
    zIndex: 1,
  },
  field: {
    backgroundColor: colors.white,
    paddingLeft: spacing.xl + spacing.s * 2,
    paddingRight: spacing.m,
    paddingVertical: spacing.m,
    borderRadius: sizes.radius,
    height: 54,
    flex: 1,
    ...shadow.light,
  },
  filterIcon: {
    position: 'absolute',
    right: spacing.s,
    zIndex: 1,
  },
});

export default SearchInput;
