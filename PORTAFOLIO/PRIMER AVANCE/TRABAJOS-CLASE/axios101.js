// Librería Axios para manejar solicitudes HTTP
const axios = require("axios")

// URL de la API JSONPlaceholder para obtener usuarios
const url = "https://jsonplaceholder.typicode.com/users"

// Ejercicio 28: Solicitar datos de usuarios mediante una solicitud GET
// Descripción: Utiliza Axios para realizar una solicitud GET a la API JSONPlaceholder y muestra el nombre de usuario de cada usuario en la respuesta.

axios.get(url).then(response => {
    response.data.forEach(element => {
        console.log(element.username)
    });
})

// Ejercicio 29: Agregar un nuevo usuario mediante una solicitud POST
// Descripción: Utiliza Axios para realizar una solicitud POST a la API JSONPlaceholder, enviando un nuevo usuario con nombre de usuario y correo electrónico. 
// La respuesta del servidor se muestra en la consola.

axios.post(url, {
    username: "Foo Bar",
    email: "foo@example.com"
}).then(response => console.log(response.data))
