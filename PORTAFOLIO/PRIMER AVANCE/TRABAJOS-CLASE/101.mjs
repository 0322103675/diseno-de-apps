// Muestra "Hola mundo xd" en la consola
console.log("Hola mundo xd");

// Declaración de variables
var s = "Foo Bar"; // Se declara una variable s con el valor "Foo Bar"
let x = 90; // Se declara una variable x con el valor 90
var y = 89; // Se declara una variable y con el valor 89

// Declaración de un array
var array = [1, 2, 3, 4, 5, "Foo", "Bar", true, false, 2.34, 4.23]; // Se crea un array con varios elementos

// Declaración de un objeto
var obj = { 
    first_name: "Foo", // Se define una propiedad first_name con el valor "Foo"
    last_name: "Bar", // Se define una propiedad last_name con el valor "Bar"
    age: 23, // Se define una propiedad age con el valor 23
    city: "TJ", // Se define una propiedad city con el valor "TJ"
    status: true , // Se define una propiedad status con el valor true
    arr: array // Se define una propiedad arr que hace referencia al array definido anteriormente
};

// Bucle for que imprime números del 0 al 99
for (let i = 0; i < 100; i++) {
    console.log(i); // Imprime el valor actual de i
}

// Bucle for que imprime cada elemento del array
for (let i = 0; i < array.length; i++) {
    console.log(array[i]); // Imprime cada elemento del array
}

// Bucle for...of incorrecto, debería ser for...of array, no array.length
for (let elem of array) {
    console.log(elem); // Imprime cada elemento del array
}

// Bucle for...of que itera a través de las claves del objeto
for(let key of Object.keys(obj)) {
    console.log(key + ": " + obj[key]); // Imprime cada propiedad del objeto y su valor
}

// Bucle for...in que itera a través de las propiedades del objeto
for(let key in obj) {
    console.log(key + ": " + obj[key]); // Imprime cada propiedad del objeto y su valor
}

// Bucle while que multiplica i por 5 hasta que sea mayor o igual a 1000
var i = 1;
while(i < 1000){
    i *= 5; // Multiplica i por 5
    console.log(i); // Imprime el valor actual de i
}

// Bucle do...while que aumenta o disminuye i hasta llegar a 100000000 o 1
var l = true;
do {
    console.log(i); // Imprime el valor actual de i
    if (i == 100000000) {
        l = false;
    } else if (i == 1) {
        l = true;
    }
    if (l) {
        i *= 10; // Multiplica i por 10
    } else {
        i /= 10; // Divide i por 10
    }
} while(true);

// Función que espera una cantidad específica de milisegundos
function esperar(milliseconds) {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
}

// Función asíncrona que espera 2 segundos antes de imprimir un mensaje
async function miFuncionConEspera() {
  console.log("Comienzo de la función"); // Imprime un mensaje indicando el inicio de la función
  await esperar(2000); // Espera 2 segundos
  console.log("Fin de la función después de esperar 2 segundos"); // Imprime un mensaje indicando el final de la función después de la espera
}

// Llamada a la función asíncrona
miFuncionConEspera();

// Reasignación de la variable s a true
s = true; // La variable s ahora tiene el valor true

// Impresión de distintas variables y expresiones
console.log(s); // Imprime el valor de la variable s
console.log(x + y); // Imprime la suma de las variables x e y
console.log(s); // Imprime el valor de la variable s
console.log(array); // Imprime el array completo
console.log(array[5]); // Imprime el elemento en la posición 5 del array
console.log(obj); // Imprime el objeto completo
console.log(obj["first_name"]); // Imprime el valor de la propiedad "first_name" del objeto
console.log(obj.arr); // Imprime el valor de la propiedad "arr" del objeto

// Estructura condicional if...else
if (x > y) {
    console.log("sí"); // Si x es mayor que y, imprime "sí"
} else {
    console.log("no"); // De lo contrario, imprime "no"
}

// Estructura condicional switch
var opc = 1;
switch (opc) {
    case 1:
        console.log("1"); // Si opc es igual a 1, imprime "1"
        break;
    case 2:
        console.log("2"); // Si opc es igual a 2, imprime "2"
        break;
    case 3:
        console.log("3"); // Si opc es igual a 3, imprime "3"
        break;
    default:
        console.log("por defecto"); // Si opc no coincide con ninguno de los casos anteriores, imprime "por defecto"
        break;
}

// Operador ternario
var animal = "Kitty";
var hello = (animal === "Kitty") ? "Es un lindo gatito" : "No es un lindo gatito";
console.log(hello); // Si animal es "Kitty", imprime "Es un lindo gatito"; de lo contrario, imprime "No es un lindo gatito"

// Función anidada
function foo() {
    var a = "gatito"; // Se declara una variable a con el valor "gatito"
    function xd() {
        console.log(a); // Imprime el valor de la variable a
    }
    xd(); // Se llama a la función xd
}

foo(); // Se llama a la función foo

// Función de cálculo del volumen de un prisma
var prisma = function(l, w, h) {
    return l * w * h; // Retorna el volumen del prisma
};
console.log("Volumen del prisma:" + prisma(23, 56, 12)); // Imprime el volumen del prisma con los valores dados

// Función que retorna otra función para calcular el volumen de un prisma
function prisma(l) {
    return function(w) {
        return function(h) {
            return l * w * h; // Retorna el volumen
        }
    }
}
