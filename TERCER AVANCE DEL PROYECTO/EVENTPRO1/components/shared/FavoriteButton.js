import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { colors, shadow, sizes } from '../../constants/theme';
import Icon from './Icon';

const FavoriteButton = ({ style, onPress }) => {
  const [active, setActive] = useState(false);

  const toggleActive = () => {
    setActive(!active);
    if (typeof onPress === 'function') {
      onPress(!active); 
    }
  };

  return (
    <TouchableOpacity style={style} onPress={toggleActive}>
      <View
        style={[
          {
            backgroundColor: colors.white,
            padding: 4,
            borderRadius: sizes.radius,
          },
          shadow.light,
        ]}
      >
        <Icon icon={active ? 'FavoriteFilled' : 'Favorite'} size={24} />
      </View>
    </TouchableOpacity>
  );
};

export default FavoriteButton;
