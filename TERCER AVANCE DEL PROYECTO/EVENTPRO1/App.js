// App.js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import OnboardingScreen from './screens/OnboardingScreen';
import LoginScreen from './screens/LoginScreen';
import SignupScreen from './screens/SignupScreen';
import MainNavigator from './navigations/MainNavigator';
import FirstScreen from './screens/FirstScreen';
import HotelsNavigator from './navigations/HotelsNavigator';

const Stack = createNativeStackNavigator();

function App() {
 return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='First'>
        <Stack.Screen name="First" component={FirstScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
        <Stack.Screen name="SignUp" component={SignupScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Onboarding" component={OnboardingScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={MainNavigator} options={{ headerShown: false }} />
        <Stack.Screen name="Hotels" component={HotelsNavigator} options={{ headerShown: false }} /> 
      </Stack.Navigator>
    </NavigationContainer>
 );
}

export default App;

